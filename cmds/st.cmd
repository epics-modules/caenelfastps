require stream,2.8.10
require caenelfastps,1.0.0test

## Load PSCH-01
iocshLoad("$(caenelfastps_DIR)/caenelfastps.iocsh", "Ch_name=SteererPSCH-01, IP_addr=172.30.4.189, P=LEBT:, R=PwrC-PSCH-01:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")

## Load PSCV-01
iocshLoad("$(caenelfastps_DIR)/caenelfastps.iocsh", "Ch_name=SteererPSCV-01, IP_addr=127.0.0.1, P=LEBT:, R=PwrC-PSCV-01:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")

## Load PSCH-02
iocshLoad("$(caenelfastps_DIR)/caenelfastps.iocsh", "Ch_name=SteererPSCH-02, IP_addr=127.0.0.1, P=LEBT:, R=PwrC-PSCH-02:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")

## Load PSCV-02
iocshLoad("$(caenelfastps_DIR)/caenelfastps.iocsh", "Ch_name=SteererPSCV-02, IP_addr=127.0.0.1, P=LEBT:, R=PwrC-PSCV-02:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")

## Run IOC
iocInit()

