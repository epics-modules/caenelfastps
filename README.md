

caenelfastps
======

European Spallation Source ERIC Site-specific EPICS module : caenelfastps 

This module provides device support for the CAENel FAST-PS family of programmable DC power supplies. 

Note that this power supply ships with an on-board EPICS IOC. Due to limited configurablity and to ensure uniform application of EPICS across the facility this IOC is not used. Instead regular IOC's shall be made using this device support module.

- Manufacturer page: https://www.caenels.com/products/fast-ps/
- User manual: ./docs/CAEN FAST-PS-Users-Manual-Rev1.5 (2).pdf
- Remote Control Manual: ./docs/Remote-Control-Manual-Rev1.2.pdf

Dependencies 
============ 
streamdevice 2.8.10 

Using the module 
================ 
In order to use the device support module declare a require statement for streamdevice and caenelfastps:
    
     require streamdevice,2.8.10
     require caenfastps,1.0.0

Then load the module:

     iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-01, IP_addr=172.30.4.189, P=LEBT:, R=PwrC-PSCH-01:, MAVVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")

Parameters: 
- Ch_name: The name of the asyn IP port connection. For trouble shooting purposes. 
- IP_addr: The IP address of your power supply LAN module 
- P: The section and subsection part of your device name (include the colon) 
- R: The discipline, device and index part of your device name (include the colon) 
- MAXVOL: The maximum voltage of your power supply (e.g. 10V) 
- MAXCUR: The maximum current of your power supply (e.g. 500A) 
- HICUR: The limit at wich a HI alarm of MINOR severity will be activated for the measured current 
- HIHICUR: The limit at wich a HIHI alarm of MAJOR severity will be activated for the measured current 

Building the module 
=================== 
This module should compile as a regular EPICS module. For instructions on how to build using E3 see the E3 documentation.

* Run `make` from the command line.
